import json
import smtplib
import datetime
import os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def testProblem(host, address, test_name, data):
    content = open("./json/problems.json", "r")
    problems = json.loads(str(content.read()))
    for problem in problems["problems"]:
        if problem["name"] in test_name.lower():
            if int(data) > problem["treshold"]:
                try:
                    lastproblem = lastProblems()
                    if lastproblem["host"] != host or lastproblem["address"] != address and lastproblem["test_name"] != test_name:
                        loggProblem(host, address, test_name, data, problem["info"])
                except:
                    loggProblem(host, address, test_name, data, problem["info"])



def lastProblems():
    lastfile = os.popen('cd log/ ; /usr/bin/ls -1t | grep "Problem" | head -1').read()
    lastfile = lastfile.strip('\n')
    content = open("./log/"+lastfile, "r")
    problems = json.loads(str(content.read()))
    return problems

def login():
    data = open("./json/auth.json", "r")
    return json.loads(str(data.read()))



def loggProblem(host, address, test_name, data, default):
    infos = "\nHOST: "+host+"\nADDRESS: "+address+"\nPROBLEM: "+default+"\n"+test_name+": "+str(data)
    js_infos = '{"host": "'+host+'", "address": "'+address+'", "problem": "'+default+'", "test_name": "'+test_name+'", "metric": '+str(data)+'}'
    datePb = str(datetime.datetime.today())
    #sendMail(infos, datePb)
    logFile(infos, js_infos, datePb)



def sendMail(infos, datePb):
    auth = login()    
    serveur = smtplib.SMTP(auth["server"], 587)
    serveur.starttls()
    serveur.login(auth["mail"], auth["password"])
    message = "From: "+auth["mail"]+"\nTo: "+auth["destination"]+"\nSubject: Probleme(s) Serveur "+datePb+"\nX-Mailer: Outlook 16.0\n\nRapport de probleme:"+infos+"\n"
    serveur.sendmail(auth["mail"],auth["destination"], message)



def logFile(infos, js_infos, datePb):
    # file = open("./log/Problem_at_"+datePb+".txt", "w+")
    file_js = open("./log/Problem_at_"+datePb+".json", "w+")
    # file.write(infos)
    file_js.write(js_infos)



#testProblem("Vcenter2", "glmsfr.ddns.net", "tmp", 70)