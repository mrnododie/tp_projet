import json
# from types import SimpleNamespace

class Oid:
    def __init__(self, oid_number, community, test_name):
        self.oid_number = oid_number
        self.community = community
        self.test_name = test_name
    
    def __str__(self):
        return str(self.oid_number) + " " + str(self.community)  + " " + str(self.test_name)
    
class Host:
    def __init__(self, hostname, ip, port = "161", oids=[]):
        self.hostname = hostname
        self.ip = ip
        self.port = port
        if len(oids) > 0 and isinstance(oids[0],dict):
            self.oids = []        
            for oid in oids:
                self.oids.append(Oid(**oid))
        else:
            self.oids = oids
        
    
    def add_oid_to_monitor(self, oid):
        self.oids.append(oid)
    
    def remove_oid_from_monitor(self, oid):
        for index, item in enumerate(self.oids, start=0):
            if item == oid:
                self.oids.pop(index)
        
    def __str__(self):
        result = str(self.hostname) +  " " + str(self.port) + " " + str(self.ip) + " "
        for item in self.oids:
            result +="\nOID:\t" + str(item)
        return result
    
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True)
