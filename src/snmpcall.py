from pysnmp.hlapi import *
from methods import *
from problem import *
from problem import *

def snmp():

    expHelp= "# HELP snmp_item"
    expType= "# TYPE snmp_item gauge"
    snmpList =""
    toList = ""

    try:
        hosts = fromJsonToObject("./json/hosts.json")
        openHost = 0

        try:   
            timeOutHost = open("./log/timed_out_host.txt", "w+")
            timeOutHost.write("")
            openLog = 0

            try:
                for host in hosts:
                    for oid in host.oids:
                        iterator = getCmd(SnmpEngine(),
                                        CommunityData(oid.community),
                                        UdpTransportTarget((host.ip, host.port)),
                                        ContextData(),
                                        ObjectType(ObjectIdentity(oid.oid_number)))

                        errorIndication, errorStatus, errorIndex, varBinds = next(iterator)
                        if errorIndication:
                            print(errorIndication)
                            toList += str(host.hostname)+" | "+str(host.ip)+" | "+str(errorIndication)+"\n"
                        else:
                            if errorStatus:
                                print('%s at %s' % (errorStatus.prettyPrint(), varBinds[int(errorIndex)-1] if errorIndex else '?'))
                                snmpList += 'snmp_item{hostname="'+host.hostname+'",hostaddress="'+host.ip+'",metric="'+oid.test_name+'",OID="'+oid.oid_number+'"} '+0+'\n'
                            else:
                                for varBind in varBinds:
                                    data= (str(varBind).split("= ",1)[1])
                                    snmpList += 'snmp_item{hostname="'+host.hostname+'",hostaddress="'+host.ip+'",metric="'+oid.test_name+'",OID="'+oid.oid_number+'"} '+data+'\n'
                                    testProblem(host.hostname, host.ip, oid.test_name, int(data))
                
                timeOutHost.write(toList)
                print(timeOutHost.read())
                expFinal = expHelp+"\n"+expType+"\n"+snmpList
                print (expFinal)
                snmpScript = 0
                return expFinal
                
            except:
                snmpScript = 1
                applicationTXT = open("./log/application_log.txt", "w+")
                applicationTXT.write(str(datetime.datetime.today())+" | An error occured with the snmpcall it self, pleas view at snmpcall.py.")
        except:
            openLog = 1
            applicationTXT = open("./log/application_log.txt", "w+")
            applicationTXT.write(str(datetime.datetime.today())+" | An error occured with the the file timed_out_host.txt, pleas verify that it exist.")

    except:
        openHost = 1
        applicationTXT = open("./log/application_log.txt", "w+")
        applicationTXT.write(str(datetime.datetime.today())+" | An error occured with host.json, please verify that it exist.")

    


snmp()
