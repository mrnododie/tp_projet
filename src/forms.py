from wtforms import Form, IntegerField, StringField, validators

class AddHostForm(Form):
    hostname = StringField('Hostname', [validators.Length(min=4, max=255)])
    ip = StringField('IP Address', [validators.Length(min=7, max=15)])
    port = IntegerField('IP Address')

class AddOidForm(Form):
    hostname = StringField('Hostname', [validators.Length(min=4, max=255)])
    oid_number = StringField('OID Number', [validators.Length(min=4, max=255)])
    community = StringField('Community', [validators.Length(min=1, max=255)])
    test_name = StringField('Test name', [validators.Length(min=1, max=255)])