# Methods.py est une librairie de méthodes spécifiquements créées pour les
# besoins de ce projet. Elles sont appelées à la fois par l'interface de configuration
# et par le moteur de l'outil de monitoring.

# Ces fonctions sont principalement utilisées pour le traitement des fichiers de configuration JSON,
# notamment en permettant de passer rapidement du modèle JSON (tableaux & dictionnaires) à un modèle
# d'objets python défini dans objects.py

from objects import *
import json, os

def addHostToJsonFile(host, filename):
    ## IF FILE is empty
    if os.stat(filename).st_size == 0:
        open(filename, "w").write(json.dumps([host], default=lambda o: o.__dict__, sort_keys=True, indent=4))
    else :
        hosts = json.loads(open(filename, "r").read())
        hosts.append(host)
        new_hosts_json = json.dumps(hosts, default=lambda o: o.__dict__, sort_keys=True, indent=4)
        open(filename, "w").write(new_hosts_json)
    
def removeHostFromJsonFile(hostname, ip, filename):
    hosts = json.loads(open(filename, "r").read())
    for i in range(len(hosts)):
        if (hosts[i]["hostname"] == hostname and hosts[i]["ip"] == ip):
            removed = hosts.pop(i)
            open(filename, "w").write(json.dumps(hosts, default=lambda o: o.__dict__, sort_keys=True, indent=4))
            break

def fromJsonToObject(hostfile):
    hosts = json.loads(open(hostfile, "r").read())
    hosts_objects = []
    for i in range(len(hosts)):
        hosts_objects.append(Host(**(hosts[i])))
    return hosts_objects
    
def fromObjectToJson(host_objects, hostfile):
    hosts_json = json.dumps(host_objects, default=lambda o: o.__dict__, sort_keys=True, indent=4)
    open(hostfile, "w").write(hosts_json)