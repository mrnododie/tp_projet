#!/bin/python3
# -*-coding:Latin-1 -*
# configInterface.py est moteur de l'interface qui permet
# de modifier le fichier de configuration qui sera lu et 
# recuperer par l'outil de monitoring.

# Ce fichier de configuration contient les infos utiles des serveurs
# à surveiller ave le monitoring, ainsi que les OIDs qui devront être 
# interrogées par ce dernier

# from os import terminal_size
from re import template
from flask import Flask, render_template, redirect, request
from methods import *
from forms import *
import json

app = Flask(__name__)
JSONFILE = "./json/hosts.json"


@app.route("/")
def hosts():
    template = "index.html"
    hosts = fromJsonToObject("./json/hosts.json")
    context = {
        "hosts" : hosts
    }
    return render_template(template, context=context)

@app.route("/about")
def about():
    template = "about.html"
    return render_template(template)

@app.route("/details-<hostname>")
def host_details(hostname):
    template = "details.html"
    selected_host = None 
    for host in fromJsonToObject("./json/hosts.json"):
        if host.hostname == hostname :
            selected_host = host
    context = { 
        "host" : selected_host
    }
    return render_template(template, context=context)

@app.route("/remove_host")
def remove_host():
    template = "remove_server.html"
    hosts = fromJsonToObject("./json/hosts.json")
    context = {
        "hosts" : hosts
    }
    return render_template(template, context=context)

@app.route("/add_host_object", methods=['GET','POST'])
def remove_host_object():
    form = AddHostForm(request.form)
    if request.method == 'POST' and form.validate():
        new_host = Host(form.hostname.data, form.ip.data,
                    form.port.data)
        print(new_host)
        addHostToJsonFile(new_host, JSONFILE)
        return redirect("/")
    return redirect('add_server.html')


@app.route("/add_host")
def add_host():
    template = "add_server.html"
    hosts = fromJsonToObject("./json/hosts.json")
    context = {
        "hosts" : hosts
    }
    return render_template(template, context=context)

@app.route("/add_host_object", methods=['GET','POST'])
def add_host_object():
    form = AddHostForm(request.form)
    if request.method == 'POST' and form.validate():
        new_host = Host(form.hostname.data, form.ip.data,
                    form.port.data)
        print(new_host)
        addHostToJsonFile(new_host, JSONFILE)
        return redirect("/")
    return redirect('add_host')

@app.route("/add_oid_object", methods=['GET','POST'])
def add_oid_object():
    form = AddOidForm(request.form)
    print(form.validate())
    if request.method == 'POST' and form.validate():
        selected_host = form.hostname.data
        new_oid = Oid(form.oid_number.data, form.community.data,
                    form.test_name.data)
        print(new_oid)

        hosts = fromJsonToObject("./json/hosts.json")
        for host in hosts:
            if host.hostname == selected_host:
                host.oids.append(new_oid)
        
        fromObjectToJson(hosts, "./json/hosts.json")
        
        return redirect("/details-" + selected_host)
    return redirect('/')

@app.route("/delete")
def remove_oid_object():
    removed_oid = request.args.get('oid', default="none", type=str)
    removed_hostname = request.args.get('hn', default="none", type=str)

    hosts = fromJsonToObject("./json/hosts.json")
    for host in hosts :
        if host.hostname == removed_hostname:
            for oid in host.oids:
                if oid.oid_number == removed_oid :
                    host.oids.pop(host.oids.index(oid))
    fromObjectToJson(hosts, "./json/hosts.json")
    return redirect(('/details-') + removed_hostname)